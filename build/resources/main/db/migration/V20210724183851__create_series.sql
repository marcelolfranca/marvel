CREATE TABLE public.series (
	id uuid NOT NULL,
	description varchar(300) NULL,
	title varchar(150) NULL,
	CONSTRAINT series_pkey PRIMARY KEY (id)
);