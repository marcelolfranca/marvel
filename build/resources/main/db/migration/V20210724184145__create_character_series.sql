CREATE TABLE public.characters_series (
	character_id uuid NOT NULL,
	series_id uuid NOT NULL,
	CONSTRAINT fk_characters_series1 FOREIGN KEY (series_id) REFERENCES public.series(id),
	CONSTRAINT fk_characters_series2 FOREIGN KEY (character_id) REFERENCES public."characters"(id)
);