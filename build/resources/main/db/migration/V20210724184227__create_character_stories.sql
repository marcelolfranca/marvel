CREATE TABLE public.characters_stories (
	character_id uuid NOT NULL,
	stories_id uuid NOT NULL,
	CONSTRAINT fk_characters_stories1 FOREIGN KEY (stories_id) REFERENCES public.stories(id),
	CONSTRAINT fk_characters_stories2 FOREIGN KEY (character_id) REFERENCES public."characters"(id)
);