CREATE TABLE public.comics (
	id uuid NOT NULL,
	description varchar(300) NULL,
	title varchar(150) NULL,
	character_id uuid NULL,
	CONSTRAINT comics_pkey PRIMARY KEY (id),
	CONSTRAINT fk_comics_characters1 FOREIGN KEY (character_id) REFERENCES public."characters"(id)
);