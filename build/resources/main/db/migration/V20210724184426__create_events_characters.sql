CREATE TABLE public.events_characters (
	events_id uuid NOT NULL,
	characters_id uuid NOT NULL,
	CONSTRAINT fk_events_characters1 FOREIGN KEY (events_id) REFERENCES public.events(id),
	CONSTRAINT fk_events_characters2 FOREIGN KEY (characters_id) REFERENCES public."characters"(id)
);