CREATE TABLE public.stories (
	id uuid NOT NULL,
	description varchar(300) NULL,
	title varchar(150) NULL,
	CONSTRAINT stories_pkey PRIMARY KEY (id)
);