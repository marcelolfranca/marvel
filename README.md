# HOW TO START THE PROJECT #

Few things will be needed to up the project.

### What is this repository for? ###
* This project was created to meet developer testing contained in the following site:
* [Marvel - Backend](https://github.com/dextra/challenges/blob/master/backend/MARVEL-BACKEND.md)
* Please visit the website above to see the endpoints available in test.

### How do I get set up? ###
* First, clone this repository.
> The project was created in the IntelliJ IDE and you should 
consider using the same IDE.  

* A instance of PostgreSQL (installation or Docker)
* Run the command below to create user and database in SQL command line:
```
CREATE USER teste WITH login password 'marvel';
CREATE database marvel;
GRANT ALL PRIVILEGES ON database marvel TO marvel;
```
* Find MarvelApplication class
* Press 'Play' button.
* Select the port 8080 to make your requests
* You can found the public entrances in '/v1/public'

All entities will be ready on ending. Now you can do requests to API.

There are a Postman's collection in 'postman' directory to help make your requests 

> **WARNING:** To run the integration tests in CharacterControllerTests is mandatory to have data in the database, provided after execution.