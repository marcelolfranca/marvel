package com.dextra.marvel.characters

import com.dextra.marvel.comics.Comic
import com.dextra.marvel.core.shared.BasicDataResponse
import com.dextra.marvel.events.Event
import com.dextra.marvel.series.Series
import com.dextra.marvel.stories.Story
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Instant
import java.util.*

class CharacterServiceTest {
    private val characterRepository = mockk<CharacterRepository>()
    private val characterService = CharacterService(characterRepository)

    val id = UUID.randomUUID()
    val comicId = UUID.randomUUID()
    val eventId = UUID.randomUUID()
    val seriesId = UUID.randomUUID()
    val storyId = UUID.randomUUID()

    val wolverine = Character(
        id = id,
        name = "Wolverine",
        description = "Wolverine is a fictional character appearing in American comic books published by Marvel Comics, mostly in association with the X-Men."
    ).apply {
        comics?.add(Comic(id = comicId, title = "The Wolverine Comic", character = this))
        events?.add(Event(id = eventId, date = Instant.now().plusSeconds(2592000), title = "The Wolverine event", characters = listOf(this)))
        series?.add(Series(id = seriesId, title = "Wolverine against the world", description = "In a changed world, he cannot realize what will do.", characters = listOf(this)))
        stories?.add(Story(id = storyId, title = "The random bullets", characters = listOf(this)))
    }

    @BeforeEach
    fun setup() {
        every { characterRepository.findById(id) }.returns(Optional.of(wolverine))
    }

    @Test
    fun `given valid id should return a character`() {
        val result = characterService.findById(id)
        Assertions.assertEquals(wolverine, result)
    }

    @Test
    fun `given a list of comics`() {
        val result = characterService.findComics(id)
        val expected = listOf(BasicDataResponse(wolverine.comics?.first()!!))
        Assertions.assertEquals(expected, result)
    }

    @Test
    fun `given a list of events`() {
        val result = characterService.findEvents(id)
        val expected = listOf(BasicDataResponse(wolverine.events?.first()!!))
        Assertions.assertEquals(expected, result)
    }

    @Test
    fun `given a list of series`() {
        val result = characterService.findSeries(id)
        val expected = listOf(BasicDataResponse(wolverine.series?.first()!!))
        Assertions.assertEquals(expected, result)
    }

    @Test
    fun `given a list of stories`() {
        val result = characterService.findStories(id)
        val expected = listOf(BasicDataResponse(wolverine.stories?.first()!!))
        Assertions.assertEquals(expected, result)
    }
}