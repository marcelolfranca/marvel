package com.dextra.marvel.characters

import com.dextra.marvel.comics.Comic
import com.dextra.marvel.comics.ComicRepository
import com.dextra.marvel.core.models.CharacterPageResponse
import com.dextra.marvel.core.shared.BasicDataResponse
import com.dextra.marvel.events.Event
import com.dextra.marvel.events.EventRepository
import com.dextra.marvel.series.Series
import com.dextra.marvel.series.SeriesRepository
import com.dextra.marvel.stories.Story
import com.dextra.marvel.stories.StoryRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpStatus
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForEntity
import java.net.URI
import java.time.Instant
import java.util.*

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CharacterControllerTest {

    @Autowired
    lateinit var characterRepository: CharacterRepository

    @Autowired
    lateinit var comicRepository: ComicRepository

    @Autowired
    lateinit var eventRepository: EventRepository

    @Autowired
    lateinit var seriesRepository: SeriesRepository

    @Autowired
    lateinit var storyRepository: StoryRepository

    @LocalServerPort
    val port: Int = 0
    val client = RestTemplate()
    val id = UUID.randomUUID()

    @BeforeAll
    fun setup() {
        val spiderMan = characterRepository.save(
            Character(
                id = id,
                name = "Spider-man",
                description = "Spider-Man is a superhero created by writer-editor Stan Lee and writer-artist Steve Ditko."
            )
        )
        comicRepository.save(Comic(title = "The Spider-Man test Comic", character = spiderMan))
        eventRepository.save(Event(date = Instant.now().plusSeconds(2592000), title = "The Spider-Man test event", characters = listOf(spiderMan)))
        seriesRepository.save(Series(title = "The fast wing - test", description = "Run Spinder-man! Run!", characters = listOf(spiderMan)))
        storyRepository.save(Story(title = "Caption this! - test", characters = listOf(spiderMan)))
    }

    @Test
    fun `should return a list of characters`() {
        val uri = URI.create("http://localhost:$port/v1/public/characters")
        val result = client.getForEntity<CharacterPageResponse>(uri)
        Assertions.assertNotNull(result.body?.data)
        Assertions.assertEquals(HttpStatus.OK, result.statusCode)
    }

    @Test
    fun `should return a list of comics`() {
        val uri = URI.create("http://localhost:$port/v1/public/characters/$id/comics")
        val result = client.getForEntity<Array<BasicDataResponse>>(uri)
        Assertions.assertNotNull(result.body?.get(0)?.id)
        Assertions.assertEquals(HttpStatus.OK, result.statusCode)
    }

    @Test
    fun `should return a list of events`() {
        val uri = URI.create("http://localhost:$port/v1/public/characters/$id/events")
        val result = client.getForEntity<Array<BasicDataResponse>>(uri)
        Assertions.assertNotNull(result.body?.get(0)?.id)
        Assertions.assertEquals(HttpStatus.OK, result.statusCode)
    }

    @Test
    fun `should return a list of series`() {
        val uri = URI.create("http://localhost:$port/v1/public/characters/$id/series")
        val result = client.getForEntity<Array<BasicDataResponse>>(uri)
        Assertions.assertNotNull(result.body?.get(0)?.id)
        Assertions.assertEquals(HttpStatus.OK, result.statusCode)
    }
}