CREATE TABLE public.characters_events (
	character_id uuid NOT NULL,
	events_id uuid NOT NULL,
	CONSTRAINT fk_characters_events1 FOREIGN KEY (events_id) REFERENCES public.events(id),
	CONSTRAINT fk_characters_events2 FOREIGN KEY (character_id) REFERENCES public."characters"(id)
);