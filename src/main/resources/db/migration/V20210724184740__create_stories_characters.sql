CREATE TABLE public.stories_characters (
	stories_id uuid NOT NULL,
	characters_id uuid NOT NULL,
	CONSTRAINT fk_stories_characters1 FOREIGN KEY (stories_id) REFERENCES public.stories(id),
	CONSTRAINT fk_stories_characters2 FOREIGN KEY (characters_id) REFERENCES public."characters"(id)
);