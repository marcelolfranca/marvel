CREATE TABLE public.series_characters (
	series_id uuid NOT NULL,
	characters_id uuid NOT NULL,
	CONSTRAINT fk_series_characters1 FOREIGN KEY (series_id) REFERENCES public.series(id),
	CONSTRAINT fk_series_characters2 FOREIGN KEY (characters_id) REFERENCES public."characters"(id)
);