CREATE TABLE public.events (
	id uuid NOT NULL,
	"date" timestamp NULL,
	title varchar(255) NULL,
	CONSTRAINT events_pkey PRIMARY KEY (id)
);