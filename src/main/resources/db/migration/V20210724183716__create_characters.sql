CREATE TABLE public."characters" (
	id uuid NOT NULL,
	"name" varchar(150) NULL,
	description varchar(300) NULL,
	CONSTRAINT characters_pkey PRIMARY KEY (id)
);