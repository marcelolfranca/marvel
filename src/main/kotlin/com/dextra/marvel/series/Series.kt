package com.dextra.marvel.series

import com.dextra.marvel.characters.Character
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "series")
data class Series(
    @Id
    val id: UUID = UUID.randomUUID(),
    val title: String,
    val description: String,
    @ManyToMany(cascade = [CascadeType.ALL])
    val characters: List<Character>
)
