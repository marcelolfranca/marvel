package com.dextra.marvel.series

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface SeriesRepository: JpaRepository<Series, UUID>