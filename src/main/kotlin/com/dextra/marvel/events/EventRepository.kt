package com.dextra.marvel.events

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface EventRepository: JpaRepository<Event, UUID>