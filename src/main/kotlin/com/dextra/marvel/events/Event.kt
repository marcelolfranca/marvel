package com.dextra.marvel.events

import java.time.Instant
import java.util.*
import com.dextra.marvel.characters.Character
import javax.persistence.*

@Entity
@Table(name = "events")
data class Event(
    @Id
    val id: UUID = UUID.randomUUID(),
    val date: Instant,
    val title: String,
    @ManyToMany(cascade = [CascadeType.ALL])
    val characters: List<Character>? = null
)
