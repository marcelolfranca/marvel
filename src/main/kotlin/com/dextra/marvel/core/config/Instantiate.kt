package com.dextra.marvel.core.config

import com.dextra.marvel.characters.Character
import com.dextra.marvel.characters.CharacterRepository
import com.dextra.marvel.comics.Comic
import com.dextra.marvel.comics.ComicRepository
import com.dextra.marvel.events.Event
import com.dextra.marvel.events.EventRepository
import com.dextra.marvel.series.Series
import com.dextra.marvel.series.SeriesRepository
import com.dextra.marvel.stories.Story
import com.dextra.marvel.stories.StoryRepository
import org.springframework.boot.CommandLineRunner
import org.springframework.context.annotation.Configuration
import java.time.Instant

@Configuration
class Instantiate(
    private val characterRepository: CharacterRepository,
    private val comicRepository: ComicRepository,
    private val eventRepository: EventRepository,
    private val seriesRepository: SeriesRepository,
    private val storyRepository: StoryRepository
    ) : CommandLineRunner {
    override fun run(vararg args: String?) {
        if (characterRepository.findAll().isEmpty()) {

            val wolverine = characterRepository.save(Character(
                    name = "Wolverine",
                    description = "Wolverine is a fictional character appearing in American comic books published by Marvel Comics, mostly in association with the X-Men."
                ))
            comicRepository.save(Comic(title = "The Wolverine Comic", character = wolverine))
            eventRepository.save(Event(date = Instant.now().plusSeconds(2592000), title = "The Wolverine event", characters = listOf(wolverine)))
            seriesRepository.save(Series(title = "Wolverine against the world", description = "In a changed world, he cannot realize what will do.", characters = listOf(wolverine)))
            storyRepository.save(Story(title = "The random bullets", characters = listOf(wolverine)))


            val spiderMan = characterRepository.save(
                Character(
                    name = "Spider-man",
                    description = "Spider-Man is a superhero created by writer-editor Stan Lee and writer-artist Steve Ditko."
                )
            )
            comicRepository.save(Comic(title = "The Spider-Man Comic", character = spiderMan))
            eventRepository.save(Event(date = Instant.now().plusSeconds(2592000), title = "The Spider-Man event", characters = listOf(spiderMan)))
            seriesRepository.save(Series(title = "The fast wing", description = "Run Spinder-man! Run!", characters = listOf(spiderMan)))
            storyRepository.save(Story(title = "Caption this!", characters = listOf(spiderMan)))

            val ironMan = characterRepository.save(
                Character(
                    name = "Iron Man",
                    description = "Inventor Tony Stark applies his genius for high-tech solutions to problems as Iron Man, the armored Avenger."
                )
            )
            comicRepository.save(Comic(title = "The Iron Man Comic", character = ironMan))
            eventRepository.save(Event(date = Instant.now().plusSeconds(2592000), title = "The Iron Man event", characters = listOf(ironMan)))
            seriesRepository.save(Series(title = "The fallen of Dr. Tris", description = "The epic reboot of IronMan.", characters = listOf(ironMan)))

            val loki = characterRepository.save(
                Character(
                    name = "Loki",
                    description = "Loki is a fictional character appearing in American comic books published by Marvel Comics."
                )
            )
            comicRepository.save(Comic(title = "The Loki Comic", character = loki))

            val blackWidow = characterRepository.save(
                Character(
                    name = "Black Widow",
                    description = "Black Widows are females who were taken at a young age to the fictional location known as Red Room to be trained as spies and assassins."
                )
            )
            comicRepository.save(Comic(title = "The Black Widow Comic", character = blackWidow))
            eventRepository.save(Event(date = Instant.now().plusSeconds(2592000), title = "The Black Widow event", characters = listOf(blackWidow)))
            seriesRepository.save(Series(title = "The russian treasure", description = "Owalvisky's betrayal.", characters = listOf(blackWidow, loki)))
            storyRepository.save(Story(title = "Don't do it!", characters = listOf(blackWidow)))

            eventRepository.save(Event(date = Instant.now().plusSeconds(2592000), title = "All characters event", characters = listOf(wolverine, spiderMan, ironMan, loki, blackWidow)))

        }
    }
}