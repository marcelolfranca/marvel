package com.dextra.marvel.core.shared

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId

fun Instant.toLocalDateTime(zoneId: String = "UTC"): LocalDateTime {
    return LocalDateTime.ofInstant(this, ZoneId.of(zoneId))
}