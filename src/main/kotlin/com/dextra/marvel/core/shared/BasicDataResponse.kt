package com.dextra.marvel.core.shared

import com.dextra.marvel.comics.Comic
import com.dextra.marvel.events.Event
import com.dextra.marvel.series.Series
import com.dextra.marvel.stories.Story
import com.fasterxml.jackson.annotation.JsonInclude
import java.time.LocalDateTime
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
data class BasicDataResponse(
    val id: UUID,
    val date: LocalDateTime? = null,
    val title: String? = null,
    val description: String? = null
) {
    constructor(comic: Comic) : this(
        id = comic.id,
        title = comic.title,
        description = comic.description
    )

    constructor(series: Series) : this(
        id = series.id,
        title = series.title,
        description = series.description
    )

    constructor(event: Event) : this(
        id = event.id,
        title = event.title,
        date = event.date.toLocalDateTime()
    )

    constructor(story: Story) : this(
        id = story.id,
        title = story.title,
        description = story.description
    )
}