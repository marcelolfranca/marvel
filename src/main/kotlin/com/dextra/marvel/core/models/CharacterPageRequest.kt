package com.dextra.marvel.core.models

import com.dextra.marvel.core.shared.PAGEABLE_LIMIT
import com.dextra.marvel.core.shared.PAGEABLE_OFFSET
import org.springframework.web.bind.annotation.RequestParam

data class CharacterPageRequest(
    @RequestParam("limit") val limit: Int = PAGEABLE_LIMIT,
    @RequestParam("offset") val offset: Int = PAGEABLE_OFFSET,
)
