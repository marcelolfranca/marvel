package com.dextra.marvel.core.models

import com.dextra.marvel.characters.Character
import java.util.*

data class CharacterResponse(
    val id: UUID = UUID.randomUUID(),
    val name: String,
    val description: String?
) {
    constructor(character: Character) : this(
        id = character.id,
        name = character.name,
        description = character.description
    )
}
