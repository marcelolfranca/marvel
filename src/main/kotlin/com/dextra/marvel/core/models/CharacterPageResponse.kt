package com.dextra.marvel.core.models

import com.dextra.marvel.core.shared.PAGEABLE_LIMIT
import com.dextra.marvel.core.shared.PAGEABLE_OFFSET
import com.dextra.marvel.core.shared.ResponsePageable

class CharacterPageResponse(
    data: List<CharacterResponse>,
    limit: Int = PAGEABLE_LIMIT,
    offset: Int = PAGEABLE_OFFSET,
    last: Boolean = false,
    count: Int = 0,
    total: Long = 0
) : ResponsePageable<CharacterResponse>(
    resource = "character",
    data = data,
    limit = limit,
    offset = offset,
    last = last,
    count = count,
    total = total
)