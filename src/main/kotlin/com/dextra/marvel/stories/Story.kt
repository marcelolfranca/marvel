package com.dextra.marvel.stories

import com.dextra.marvel.characters.Character
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "stories")
data class Story(
    @Id
    val id: UUID = UUID.randomUUID(),
    val title: String,
    val description: String? = null,
    @ManyToMany(cascade = [CascadeType.ALL])
    val characters: List<Character>
)
