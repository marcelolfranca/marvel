package com.dextra.marvel.comics

import com.dextra.marvel.characters.Character
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "comics")
data class Comic(
    @Id
    val id: UUID = UUID.randomUUID(),
    val title: String,
    val description: String? = null,
    @ManyToOne
    val character: Character
)
