package com.dextra.marvel.comics

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface ComicRepository: JpaRepository<Comic, UUID>