package com.dextra.marvel.characters

import com.dextra.marvel.core.exceptions.CharacterNotFoundException
import com.dextra.marvel.core.models.CharacterPageRequest
import com.dextra.marvel.core.models.CharacterPageResponse
import com.dextra.marvel.core.models.CharacterResponse
import com.dextra.marvel.core.shared.BasicDataResponse
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import java.util.*

@Service
class CharacterService(private val characterRepository: CharacterRepository) {

    fun findAll(request: CharacterPageRequest): CharacterPageResponse {
        val pageable = PageRequest.of(request.offset, request.limit)
        val pagedCharacters = characterRepository.findAll(pageable)
        val data = pagedCharacters.content.map { CharacterResponse(it) }
        return CharacterPageResponse(
            data = data,
            limit = request.limit,
            offset = request.offset,
            last = pagedCharacters.isLast,
            count = pagedCharacters.count(),
            total = pagedCharacters.totalElements
        )
    }

    fun findCharById(id: UUID): CharacterResponse {
        val character = characterRepository.findById(id).orElseThrow { CharacterNotFoundException("Character with ID $id not found") }
        return CharacterResponse(character)
    }

    fun findById(id: UUID): Character {
        return characterRepository.findById(id).orElseThrow { CharacterNotFoundException("Character with ID $id not found") }
    }

    fun findComics(characterId: UUID): List<BasicDataResponse> {
        val character = findById(characterId)
        return character.comics?.map { BasicDataResponse(it) } ?: listOf()
    }

    fun findSeries(characterId: UUID): List<BasicDataResponse> {
        val character = findById(characterId)
        return character.series?.map { BasicDataResponse(it) } ?: listOf()
    }

    fun findStories(characterId: UUID): List<BasicDataResponse> {
        val character = findById(characterId)
        return character.stories?.map { BasicDataResponse(it) } ?: listOf()
    }

    fun findEvents(characterId: UUID): List<BasicDataResponse> {
        val character = findById(characterId)
        return character.events?.map { BasicDataResponse(it) } ?: listOf()
    }
}