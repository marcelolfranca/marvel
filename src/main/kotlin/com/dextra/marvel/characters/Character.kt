package com.dextra.marvel.characters

import com.dextra.marvel.comics.Comic
import com.dextra.marvel.events.Event
import com.dextra.marvel.series.Series
import com.dextra.marvel.stories.Story
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "characters")
data class Character(
    @Id
    val id: UUID = UUID.randomUUID(),
    val name: String,
    val description: String?,
    @OneToMany(cascade = [CascadeType.ALL], mappedBy = "character")
    val comics: MutableList<Comic>? = mutableListOf(),
    @ManyToMany(cascade = [CascadeType.ALL], mappedBy = "characters")
    val events: MutableList<Event>? = mutableListOf(),
    @ManyToMany(cascade = [CascadeType.ALL], mappedBy = "characters")
    val series: MutableList<Series>? = mutableListOf(),
    @ManyToMany(cascade = [CascadeType.ALL], mappedBy = "characters")
    val stories: MutableList<Story>? = mutableListOf()
)
