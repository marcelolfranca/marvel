package com.dextra.marvel.characters

import com.dextra.marvel.core.models.CharacterPageRequest
import com.dextra.marvel.core.models.CharacterPageResponse
import com.dextra.marvel.core.models.CharacterResponse
import com.dextra.marvel.core.shared.BasicDataResponse
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("characters")
class CharacterController(private val characterService: CharacterService) {
    @GetMapping
    fun findAll(request: CharacterPageRequest): ResponseEntity<CharacterPageResponse> {
        val response = characterService.findAll(request)
        return ResponseEntity.ok(response)
    }

    @GetMapping("{characterId}")
    fun findById(@PathVariable("characterId") characterId: UUID): ResponseEntity<CharacterResponse> {
        val response = characterService.findCharById(characterId)
        return ResponseEntity.ok(response)
    }

    @GetMapping("{characterId}/comics")
    fun findComics(@PathVariable("characterId") characterId: UUID): ResponseEntity<List<BasicDataResponse>> {
        val response = characterService.findComics(characterId)
        return ResponseEntity.ok(response)
    }

    @GetMapping("{characterId}/events")
    fun findEvents(@PathVariable("characterId") characterId: UUID): ResponseEntity<List<BasicDataResponse>> {
        val response = characterService.findEvents(characterId)
        return ResponseEntity.ok(response)
    }

    @GetMapping("{characterId}/series")
    fun findSeries(@PathVariable("characterId") characterId: UUID): ResponseEntity<List<BasicDataResponse>> {
        val response = characterService.findSeries(characterId)
        return ResponseEntity.ok(response)
    }

    @GetMapping("{characterId}/stories")
    fun findStories(@PathVariable("characterId") characterId: UUID): ResponseEntity<List<BasicDataResponse>> {
        val response = characterService.findStories(characterId)
        return ResponseEntity.ok(response)
    }
}