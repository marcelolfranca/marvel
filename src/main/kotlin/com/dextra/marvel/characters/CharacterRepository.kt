package com.dextra.marvel.characters

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface CharacterRepository: JpaRepository<Character, UUID>, PagingAndSortingRepository<Character, UUID>